package Model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MySQLDAO {

    public static final String DRIVER = "com.mysql.jdbc.Driver";
    public static final String DBURL = "jdbc:mysql:///EngSw";
    private static Connection con;

    @SuppressWarnings(value = {"CallToPrintStackTrace"}) 
    public static Connection getConnection() {
        if (con == null) {
            try {
                Class.forName(DRIVER).newInstance();
                con = DriverManager.getConnection(DBURL, "root", "JDP123");
            } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | SQLException e) {
                e.printStackTrace();
            }
        }
        return con;
    }

    @SuppressWarnings(value = {"CallToPrintStackTrace"}) 
    public static ResultSet getResulSet(String query, Object... parametros) {
        PreparedStatement psmt;
        ResultSet rs = null;
        try {
            psmt = con.prepareStatement(query);
            rs = psmt.executeQuery();
            for (int i = 0; i < parametros.length; i++) {
                psmt.setObject(i + 1, parametros[i]);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rs;
    }

    @SuppressWarnings(value = {"CallToPrintStackTrace"}) 
    public static long executeQuery(String query, Object... parametros) {
        long update = 0;
        PreparedStatement psmt;
        try {
            psmt = con.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
            for (int i = 0; i < parametros.length; i++) {
                psmt.setObject(i + 1, parametros[i]);
            }
            psmt.execute();
            ResultSet rs = psmt.getGeneratedKeys();
            if (rs != null && rs.next()) {
                update = rs.getLong(1);
            }
            psmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return update;
    }

    @SuppressWarnings(value = {"CallToPrintStackTrace"}) 
    public static void terminar() {
        try {
            (MySQLDAO.getConnection()).close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
