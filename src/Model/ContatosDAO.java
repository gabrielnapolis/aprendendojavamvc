package Model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ContatosDAO {

    private static ContatosDAO instance;

    private ContatosDAO() {
        MySQLDAO.getConnection();
    }

    public static ContatosDAO getInstance() {
        if (instance == null) {
            instance = new ContatosDAO();
        }
        return instance;
    }

    public long create(ContatosBEAN contato) {
        String query = "INSERT INTO CONTATOS (nome, apelido, data_nascimento)"
                + " VALUES (?,?,?)";
        return MySQLDAO.executeQuery(query, contato.getNome(), contato.getApelido(), contato.getData_nascimento());
    }

    public ArrayList<ContatosBEAN> retrieve() {
        String query = "SELECT * FROM Contatos ORDER BY Nome";
        return listarContatos(query);
    }

    public void update(ContatosBEAN contato) {
        String query = "UPDATE CONTATOS SET nome=?, apelido=?, data_nascimento=? WHERE id = ?";
        MySQLDAO.executeQuery(query, contato.getNome(), contato.getApelido(), contato.getData_nascimento(), contato.getId());
    }

    public void delete(ContatosBEAN contato) {
        String query = "DELETE FROM CONTATOS WHERE id = ?";
        MySQLDAO.executeQuery(query, contato.getId());
    }

    @SuppressWarnings(value = {"CallToPrintStackTrace"})
    public ArrayList<ContatosBEAN> listarContatos(String query) {
        ArrayList<ContatosBEAN> lista = new ArrayList<>();
        try {
            ResultSet rs = MySQLDAO.getResulSet(query);
            while (rs.next()) {
                lista.add(new ContatosBEAN(rs.getInt("id"), rs.getString("nome"), rs.getString("apelido"), rs.getDate("data_nascimento")));
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lista;
    }

    @SuppressWarnings(value = {"CallToPrintStackTrace"})
    public ContatosBEAN buscarContato(int id) {
        try {
            ResultSet rs = MySQLDAO.getResulSet("SELECT * FROM Contatos WHERE id=?");
            if (rs.next()) {
                return new ContatosBEAN(rs.getInt("id"), rs.getString("nome"), rs.getString("apelido"), rs.getDate("data_nascimento"));
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean seExiste(int id) {
        if (buscarContato(id) == null) {
            return false;
        } else {
            return true;
        }
    }
}
