package Control;

import Model.ContatosBEAN;
import java.awt.HeadlessException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JOptionPane;

public class MenuPrincipal {

    ArrayList<ContatosBEAN> listaContatos = new ArrayList<>();

    public Date converterStringParaData(String dataString) {
        try {
            SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
            return new java.sql.Date(formato.parse(dataString).getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void iniciar() {
        int opcao = 1;
        while (opcao != 0) {
            String texto = ""
                    + "1) Inserir\n"
                    + "2) Alterar\n"
                    + "3) Excluir\n"
                    + "4) Consultar\n"
                    + "5) Listar\n"
                    + "0) Sair";
            try {
                opcao = Integer.parseInt(JOptionPane.showInputDialog(texto));
            } catch (HeadlessException | NumberFormatException ex) {
                opcao = -1;
            }
            switch (opcao) {
                case 0:
                    sair();
                    break;
                case 1:
                    inserir();
                    break;
                case 2:
                    alterar();
                    break;
                case 3:
                    excluir();
                    break;
                case 4:
                    consultar();
                    break;
                case 5:
                    listar();
                    break;
                default:
                    JOptionPane.showMessageDialog(null, "Digito inválido!");
                    break;
            }
        }
    }

    private void sair() {
        System.exit(0);
    }

    private void inserir() {
       
    }
    
    private void alterar(){
        
    }

    private void excluir(){
        
    }
    
    private void consultar(){
        
    }
    
    private void listar(){
        
    }
}
