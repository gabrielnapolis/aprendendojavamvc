package Control;

import Model.ContatosBEAN;
import Model.ContatosDAO;
import java.util.ArrayList;

public class ContatoControl {

    public void adicionarContato(ContatosBEAN contato) {
        ContatosDAO.getInstance().create(contato);
    }

    public ArrayList<ContatosBEAN> listarContatos() {
        return ContatosDAO.getInstance().retrieve();
    }

    public void atualizarContato(ContatosBEAN contato) {
        ContatosDAO.getInstance().update(contato);
    }

    public void deletarContato(ContatosBEAN contato) {
        ContatosDAO.getInstance().delete(contato);
    }

    public ContatosBEAN buscarContato(int id) {
        return ContatosDAO.getInstance().buscarContato(id);
    }

    public boolean seExiste(int id) {
        return ContatosDAO.getInstance().seExiste(id);
    }

}
